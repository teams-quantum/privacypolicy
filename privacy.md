# Privacy Policy for the Chrome Extension: Profile To Notion
This Privacy Policy outlines how Profile To Notion ("we", "us", "our", or the "Company") collects, uses, and protects user data through the Chrome extension named Profile To Notion (the "Extension"). This policy is intended to inform you about what personal data we collect, how it is used, stored, and shared, as well as your choices regarding the use of your personal information. </br>

## What Information Do We Collect?

<b>1. User Provided Information:</b> When using the Extension, we collect the following information that you provide to us voluntarily: </br>

&emsp;<b>1. Your email address:</b> We collect your email address to uniquely identify you as a user of the Extension. </br>

&emsp;<b>2. Your Notion details:</b> To save LinkedIn profile data to your Notion database, we collect the necessary Notion details that you provide us. </br>

## How Do We Use Your Information?

<b>Communication:</b> We may use your email address to communicate with you about the Extension, such as sending you updates, announcements, and notifications. </br>

<b>Analytics:</b> We may use aggregated and anonymized data for analytics and research purposes to understand how users interact with the Extension and to improve its features and performance. </br>

## How Is Your Information Stored and Protected?

<b>Data Storage:</b> Your information is stored on secure servers operated by Profile To Notion or its third-party service providers. </br>

<b>Data Security:</b> We implement and maintain appropriate technical and organizational security measures to protect your information from unauthorized access, use, or disclosure. </br>

## How Is Your Information Shared?

Third-party Service Providers: We may share your information with third-party service providers who assist us in providing and improving the Extension, such as cloud storage providers.
Legal Compliance: We may disclose your information if required to do so by law or in response to valid requests by public authorities (e.g., law enforcement or government agencies).

## Your Choices Regarding Your Information

<b>Deletion:</b> You may request the deletion of your personal information by contacting us at <b>support@profiletonotion.app</b>. </br>

<b>Opt-Out:</b> You may opt-out of receiving communications from us by following the unsubscribe instructions provided in our communications or by contacting us at <b>support@profiletonotion.app</b>. </br>

## Changes to This Privacy Policy

We may update this Privacy Policy from time to time to reflect changes in our practices or for other operational, legal, or regulatory reasons. We encourage you to review this Privacy Policy periodically to stay informed about how we collect, use, and protect your information. </br>
## Contact Us
If you have any questions, comments, or concerns about this Privacy Policy or our practices regarding your personal information, please contact us at <b>support@profiletonotion.app</b>. </br>

Last Updated: 23-Feb-2024
